use [NextNopCommerce]

GO

IF NOT EXISTS (SELECT 1 FROM [Store] WHERE [name] = N'localhost')
BEGIN
INSERT INTO [dbo].[Store]
           ([Name]
           ,[Url]
           ,[SslEnabled]
           ,[Hosts]
           ,[DefaultLanguageId]
           ,[DisplayOrder]
           ,[CompanyName]
           ,[CompanyAddress]
           ,[CompanyPhoneNumber]
           ,[CompanyVat])
     VALUES
           ('localhost'
           ,'http:localhost:15536'
           ,0
           ,'localhost'
           ,0
           ,1
           ,'todo'
           ,'todo'
           ,''
           ,'')
END

GO

IF NOT EXISTS (SELECT 1 FROM [Language] WHERE [name] = N'English')
BEGIN
INSERT INTO [dbo].[Language]
           ([Name]
           ,[LanguageCulture]
           ,[UniqueSeoCode]
           ,[FlagImageFileName]
           ,[Rtl]
           ,[LimitedToStores]
           ,[Published]
           ,[DisplayOrder]
           ,[DefaultCurrencyId])
     VALUES
           ('English'
           ,'en-us'
           ,'en'
           ,'us.png'
           ,0
           ,0
           ,1
           ,1
           ,0)
END

GO

IF NOT EXISTS (SELECT 1 FROM [CustomerRole] WHERE [name] = N'Administrator')
BEGIN
INSERT INTO [dbo].[CustomerRole]
           ([Name]
           ,[FreeShipping]
           ,[TaxExempt]
           ,[Active]
           ,[IsSystemRole]
           ,[SystemName]
           ,[PurchasedWithProductId]
           ,[EnablePasswordLifetime]
           ,[OverrideTaxDisplayType]
           ,[DefaultTaxDisplayTypeId])
     VALUES
           ('Administrator'
           ,0
           ,0
           ,1
           ,1
           ,'Administrators'
           ,0
           ,0
           ,0
           ,0)
END

IF NOT EXISTS (SELECT 1 FROM [CustomerRole] WHERE [name] = N'Registered')
BEGIN
INSERT INTO [dbo].[CustomerRole]
           ([Name]
           ,[FreeShipping]
           ,[TaxExempt]
           ,[Active]
           ,[IsSystemRole]
           ,[SystemName]
           ,[PurchasedWithProductId]
           ,[EnablePasswordLifetime]
           ,[OverrideTaxDisplayType]
           ,[DefaultTaxDisplayTypeId])
     VALUES
           ('Registered'
           ,0
           ,0
           ,1
           ,1
           ,'Registered'
           ,0
           ,0
           ,0
           ,0)
END

GO

IF NOT EXISTS (SELECT 1 FROM [CustomerRole] WHERE [name] = N'Guests')
BEGIN
INSERT INTO [dbo].[CustomerRole]
           ([Name]
           ,[FreeShipping]
           ,[TaxExempt]
           ,[Active]
           ,[IsSystemRole]
           ,[SystemName]
           ,[PurchasedWithProductId]
           ,[EnablePasswordLifetime]
           ,[OverrideTaxDisplayType]
           ,[DefaultTaxDisplayTypeId])
     VALUES
           ('Guests'
           ,0
           ,0
           ,1
           ,1
           ,'Guests'
           ,0
           ,0
           ,0
           ,0)
END           

GO

